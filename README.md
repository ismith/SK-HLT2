LHCb ImpactKit:

HLT2 Line project

Setup the envirement and get the Hlt/Hlt2Lines package

```
lb-dev --nightly-cvmfs --nightly lhcb-head thu Moore HEAD

git lb-use Hlt

git lb-checkout Hlt/SK-DIElec Hlt/Hlt2Lines
git lb-checkout Hlt/SK-DIElec Hlt/HltSettings
git lb-checkout Hlt/SK-DIElec Hlt/Hlt2SharedParticles
```